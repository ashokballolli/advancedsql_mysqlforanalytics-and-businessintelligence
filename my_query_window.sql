-- SET GLOBAL max_allowed_packet = 1073741824;
-- show variables;

-- ERROR 1292 (22007) at line 114: Incorrect datetime value: '2012-03-25 02:23:06' for column 'created_at' at row 898
use mavenfuzzyfactory;
select * from website_sessions order by website_session_id desc limit 10;

-- Traffice source analysis

select utm_content, count(distinct website_session_id) as count_sessions
from website_sessions
where website_session_id between 1000 and 2000
group by utm_content
order by count_sessions desc;

select utm_content, count(distinct website_session_id) as count_sessions
from website_sessions
where website_session_id between 1000 and 2000
group by 1
order by 2 desc;

select 
	ws.utm_content, 
    count(distinct ws.website_session_id) as count_sessions,
    count(distinct o.order_id) as orders,
    count(distinct o.order_id)/count(distinct ws.website_session_id) as session_to_order_conv_rt
from website_sessions ws
	left join orders o
		on ws.website_session_id = o.website_session_id
where ws.website_session_id between 1000 and 2000
group by ws.utm_content
order by count_sessions desc;

-- Assignment 1: 
select 
	utm_source, 
	utm_campaign, 
    http_referer, 
    count(distinct website_session_id) as sessions
from website_sessions
where created_at < '2012-04-12'
group by 
	utm_source, 
    utm_campaign, 
    http_referer
order by sessions desc;

-- Assignment 2:

select count(distinct website_sessions.website_session_id) as sessions, 
		count(distinct orders.order_id) as orders,
        100*(count(distinct orders.order_id)/count(distinct website_sessions.website_session_id)) as session_to_order_conv_rate
from website_sessions left join orders
	on orders.website_session_id = website_sessions.website_session_id
where website_sessions.created_at < '2012-04-14'
	and website_sessions.utm_source = 'gsearch'
    and website_sessions.utm_campaign = 'nonbrand';
    
  
--    

select year(created_at) as yr,
	week(created_at) as wek,
    min(date(created_at)) as start_of_week,
    count(distinct website_session_id) as sessions
from website_sessions
where website_session_id between 100000 and 115000
group by yr, wek;



    
    