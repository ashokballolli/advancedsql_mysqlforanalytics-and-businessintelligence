-- Section-7: Analysis for channel portfolio management


select 
	wss.utm_content,
	count(distinct wss.website_session_id) as sessions,
    count(distinct ord.order_id) as orders,
    (count(distinct ord.order_id)/count(distinct wss.website_session_id))*100 as converstion_rate
from website_sessions wss
	left join orders ord
    on ord.website_session_id = wss.website_session_id
where
	wss.created_at between '2014-01-01' and '2014-02-01'
group by
	wss.utm_content
order by
	sessions desc
    ;
    
-- assignment 55 -    weekly trended session volume and compare to gsearch nonbrand
-- With gsearch doing well and the site performing better, we launched a second paid search channel, bsearch, around August 22.
-- Can you pull weekly trended session volume since then and compare to gsearch nonbrand so I can get a sense for how important this will be for the business?

select distinct utm_source from website_sessions limit 100;

select min(date(created_at)) as week_start_date,
	count(distinct(case when utm_source = 'gsearch' then website_session_id end)) as gsearch_sessions,
    count(distinct(case when utm_source = 'bsearch' then website_session_id end)) as bsearch_sessions
from website_sessions
where created_at between '2012-08-22' and '2012-11-29'
	and utm_campaign = 'nonbrand'
group by year(created_at), week(created_at)
;

-- assignment 57
-- I’d like to learn more about the bsearch nonbrand campaign. Could you please pull the percentage of traffic coming on Mobile, and compare that to gsearch?
-- Feel free to dig around and share anything else you find interesting. Aggregate data since August 22nd is great, no need to show trending at this point.


select utm_source,
	-- total_sessions
    count(distinct website_session_id) as sessions,
	-- mobile_sessions
    count(distinct(case when device_type = 'mobile' then website_session_id end)) as mobile_sessions,
    -- pct_mobile
    (count(distinct(case when device_type = 'mobile' then website_session_id end))/count(distinct website_session_id)) * 100 as pct_mobile
from website_sessions
where
	created_at between '2012-08-22' and '2012-11-30' and
    utm_campaign = 'nonbrand'
group by 
	utm_source
;

-- assignment 59
-- I’m wondering if bsearch nonbrand should have the same bids as gsearch. Could you pull nonbrand conversion rates from session to order for gsearch and bsearch, and slice the data by device type?
-- Please analyze data from August 22 to September 18; we ran a special pre-holiday campaign for gsearch starting on September 19th, so the data after that isn’t fair game.

select website_sessions.device_type,
	website_sessions.utm_source,
	-- total_sessions
    count(distinct website_sessions.website_session_id) as sessions,
	-- orders
    count(distinct(orders.order_id)) as orders,
    -- conv_rate
     count(distinct(orders.order_id)) / count(distinct website_sessions.website_session_id) as conv_rate
from website_sessions left join orders
	on website_sessions.website_session_id = orders.website_session_id
where
	website_sessions.created_at between '2012-08-22' and '2012-09-19' and
    website_sessions.utm_campaign = 'nonbrand'
group by 
	website_sessions.utm_source, website_sessions.device_type
    ;
    
-- assignment 61
-- Based on your last analysis, we bid down bsearch nonbrand on December 2nd. Can you pull weekly session volume for gsearch and bsearch nonbrand, broken down by device, since November 4th? 
-- If you can include a comparison metric to show bsearch as a percent of gsearch for each device, that would be great too.

select min(created_at) as week_start_date,
	-- device_level_sessions
    count(distinct(case when device_type = ))
from website_sessions
where 
	website_sessions.created_at between '2012-11-04' and '2012-12-22' and
    website_sessions.utm_campaign = 'nonbrand'
group by
	week(created_at)