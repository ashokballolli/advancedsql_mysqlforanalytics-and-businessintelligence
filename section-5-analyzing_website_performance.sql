use mavenfuzzyfactory;

select * 
from website_pageviews
where website_pageview_id< 1000
;

-- largest volumne page

select pageview_url,
	count(distinct website_pageview_id) as views
from website_pageviews
where website_pageview_id < 1000 -- arbitrary
group by pageview_url
order by views desc
;

-- top entry pages from users
-- created_at or autoincrement column can be used to find out the first entry page of a user(i.e. website-session-id)


create temporary table first_pageview
select website_session_id,
	min(website_pageview_id) as min_pv_id
from website_pageviews
where website_pageview_id < 1000
group by website_session_id
;

select fpv.website_session_id,
	wpv.pageview_url as landing_page -- aka 'entry page'
from first_pageview fpv left join website_pageviews wpv
	on fpv.min_pv_id = wpv.website_pageview_id
;

-- group and count by page urls

select wpv.pageview_url as landing_page, -- aka 'entry page'
	count(distinct fpv.website_session_id) as session_hitting_this_lander
from first_pageview fpv left join website_pageviews wpv
	on fpv.min_pv_id = wpv.website_pageview_id
group by wpv.pageview_url
;

-- assignment 34 - most viewed website pages, ranked by session volume

select pageview_url,
	count(distinct website_pageview_id) as pvs
from website_pageviews
where created_at < '2012-06-09'
group by pageview_url
order by pvs desc
;

-- assignment 36 - pull all entry pages and rank them on entry volume
-- step-1: find the first page view for each session
-- step-2: find the url the customer saw on that first pageview

create temporary table website_landingpage_by_session
select min(website_pageview_id) as pvid,
	website_session_id as wsid
from website_pageviews
where created_at < '2012-06-12'
group by website_session_id
;

select wpv.pageview_url as landing_page_url,
	count(distinct wlbs.pvid) as sessions_hitting_page
from website_landingpage_by_session wlbs
	left join website_pageviews wpv
    on wlbs.pvid = wpv.website_pageview_id
-- where wpv.created_at < '2012-06-12'    
group by wpv.pageview_url
;

-- select count(distinct website_pageview_id) from website_pageviews where created_at < '2012-06-12' and pageview_url = '/home';