-- 
select * from orders order by order_id desc limit 10 ;

 -- number of orders by product od
 select count(distinct order_id) as no_orders, primary_product_id
 from orders
 where order_id between 31000 and 32000
 group by primary_product_id;
 
 -- numner of 1 item and 2 item orders by product type
 select primary_product_id,
		count(distinct case when items_purchased = 1 then order_id else null end) as single_item_orders,
        count(distinct case when items_purchased = 2 then order_id else null end) as two_item_orders,
        (count(distinct case when items_purchased = 1 then order_id else null end) + count(distinct case when items_purchased = 2 then order_id else null end)) as total_orders
 from orders
 where order_id between 31000 and 32000
 group by primary_product_id
 ;
 
 
 -- assignment on traffic source trending
 -- gsearch nonbrand trended session volume, by week
 
 select
	-- week(created_at) as yr, 
    -- year(created_at) as wk,
	min(date(created_at)) as week_start_date,
	count(distinct(website_session_id)) as sessions
 from website_sessions
 where created_at < '2012-05-10' 
	and utm_source = 'gsearch' 
	and utm_campaign = 'nonbrand'
 group by 
	week(created_at), 
    year(created_at)
;

-- assignment on gsearch device level performance
-- conversion rates from session to order by device type

select ws.device_type,
	count(distinct(ws.website_session_id)) as sessions,
    count(distinct(od.order_id)) as orders,
    (count(distinct(od.order_id)) / count(distinct(ws.website_session_id))) as session_to_order_conv_rate
from website_sessions ws left join orders od
	on ws.website_session_id = od.website_session_id
where
	ws.created_at < '2012-05-11' and -- this was wrong, was using created_date from orders, so always it gives the onversion rate as 1
    ws.utm_campaign = 'nonbrand' and
    ws.utm_source = 'gsearch'
group by
		ws.device_type
        ;
    
 -- assignment -- weekly trends for desktop and mobile
 -- always write the query without deviding by cateogry with the basic grouping and notedown the results then compare the final query sum results with this to veriy
 
 -- below is for verifying the results at the end, not the actual query for the assignment
 
select count(distinct website_session_id) as sessions,
	year(created_at) as yr,
    week(created_at) as wk,
    min(date(created_at)) as week_start_date
from website_sessions
where created_at < '2012-06-09' and
		created_at >= '2012-04-15' and
	    utm_campaign = 'nonbrand' and
		utm_source = 'gsearch'
group by year(created_at), week(created_at)
        ;
        
-- below is the query for the assignment, compare the total numbers to verify the results are correct, can also verify it indiudually by adding conditions in where like < created_at < for a particular week then get the total numbers, group by device type
select min(date(created_at)) as week_start_date,
		count(distinct(case when device_type = 'desktop' then website_session_id else null end)) as dtop_sessions,
        count(distinct(case when device_type = 'mobile' then website_session_id else null end)) as mob_sessions,
		(count(distinct(case when device_type = 'desktop' then website_session_id else null end)) + count(distinct(case when device_type = 'mobile' then website_session_id else null end))) as total_sessions
from website_sessions
 where created_at < '2012-06-09' and
		created_at >= '2012-04-15' and
	    utm_campaign = 'nonbrand' and
		utm_source = 'gsearch'
group by year(created_at), week(created_at)