use mavenfuzzyfactory;

select * 
from website_pageviews
where website_pageview_id< 1000
;

-- largest volumne page

select pageview_url,
	count(distinct website_pageview_id) as views
from website_pageviews
where website_pageview_id < 1000 -- arbitrary
group by pageview_url
order by views desc
;

-- top entry pages from users
-- created_at or autoincrement column can be used to find out the first entry page of a user(i.e. website-session-id)


create temporary table first_pageview
select website_session_id,
	min(website_pageview_id) as min_pv_id
from website_pageviews
where website_pageview_id < 1000
group by website_session_id
;

select fpv.website_session_id,
	wpv.pageview_url as landing_page -- aka 'entry page'
from first_pageview fpv left join website_pageviews wpv
	on fpv.min_pv_id = wpv.website_pageview_id
;

-- group and count by page urls

select wpv.pageview_url as landing_page, -- aka 'entry page'
	count(distinct fpv.website_session_id) as session_hitting_this_lander
from first_pageview fpv left join website_pageviews wpv
	on fpv.min_pv_id = wpv.website_pageview_id
group by wpv.pageview_url
;